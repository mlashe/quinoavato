--[[
Quinoa Vato Created by: Monitrice Turner
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

--there are mulitple files to include
require 'code/lib/misc'
require 'code/lib/anall'
require 'code/lib/middleclass'
require 'code/lib/misc'
require 'code/src/game'
require 'code/src/player'
require 'code/src/goodFood'
require 'code/src/goodFood2'
require 'code/src/badFood'
require 'code/src/badFood1'
require 'code/src/container'
require 'code/src/director'




--callback (once game is loaded)
function love.load()
  
  
 -- canvas
  nativeWindowWidth = 1280
  nativeWindowHeight = 720

  nativeCanvasWidth = 1280
  nativeCanvasHeight = 720

  canvas = love.graphics.newCanvas(nativeCanvasWidth, nativeCanvasHeight)
  canvas:setFilter('linear', 'linear', 2) 
 
 
 --want to keep score
  --score = 0
  
  --assets
  --fonts
  fonts = {}
  fonts.large = love.graphics.newFont("assets/fonts/gameplayfont.ttf", 36)
  
  --sounds
  sounds = {}
  sounds.footstep = love.audio.newSource("assets/sounds/footstep.WAV", "static")
  sounds.footstep:setVolume(0.25)
  sounds.goodFood = love.audio.newSource("assets/sounds/crunch.WAV", "static")
  sounds.goodFood2 = love.audio.newSource("assets/sounds/mmm.mp3", "static")
  sounds.badFood  = love.audio.newSource("assets/sounds/oops.WAV", "static")
  sounds.badFood1  = love.audio.newSource("assets/sounds/oops.WAV", "static")
  sounds.playermove = love.audio.newSource("assets/sounds/footstep.WAV", "static")
  sounds.bawselvl = love.audio.newSource("assets/sounds/bawselvl.WAV", "static")
  sounds.bawselvl:setVolume(0.25)
   
    --music
  music = {}
  music.main = love.audio.newSource("assets/music/backgroundSalsa.wav", "stream")
  music.main:setVolume(0.10)
  music.main:setLooping(true)
  music.main:play()

  --images
  images = {}
  images.background = love.graphics.newImage("assets/images/floor.jpg")
  images.goodFood   = love.graphics.newImage("assets/images/foods/goodFood/apple.png")
  images.goodFood2  = love.graphics.newImage("assets/images/foods/goodFood/greenbeans.png")
  images.badFood    = love.graphics.newImage("assets/images/foods/badFood/taco.png")
  images.badFood1   = love.graphics.newImage("assets/images/foods/badFood/cupcake.png")
  
  -- animations
  animations = {}
  animations.player = {}
  local frameTime = 0.15
    animations.player.up = newAnimation(love.graphics.newImage('assets/images/player/up.png'), 128, 128,   frameTime, 4)
    animations.player.down = newAnimation(love.graphics.newImage('assets/images/player/down.png'), 128, 128, frameTime, 4)
    animations.player.left = newAnimation(love.graphics.newImage('assets/images/player/left.png'), 128, 128, frameTime, 4)
    animations.player.right = newAnimation(love.graphics.newImage('assets/images/player/right.png'), 128, 128, frameTime, 4)
  
  
  
  
  -- seed random function
  math.randomseed(os.time())  
    
  game = Game:new('Menu')
  
end
 

function love.update(dt)
  
      -- determine window scale and offset
    windowScaleX = love.graphics.getWidth() / nativeWindowWidth
    windowScaleY = love.graphics.getHeight() / nativeWindowHeight
    windowScale =  math.min(windowScaleX, windowScaleY)
    windowOffsetX = round((windowScaleX - windowScale) * (nativeWindowWidth * 0.5))
    windowOffsetY = round((windowScaleY - windowScale) * (nativeWindowHeight * 0.5))

    -- update game
    game:update(dt)
end
 
-- Draw a coloured rectangle.
function love.draw()
  
    -- draw everything to canvas of native size, then upscale and offset
    love.graphics.setCanvas(canvas)
    game:draw()
    love.graphics.setCanvas()
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(canvas, windowOffsetX, windowOffsetY, 0, windowScale, windowScale)

    -- letterboxing
    local windowWidth = love.graphics.getWidth()
    local windowHeight = love.graphics.getHeight()
    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle('fill', 0, 0, windowOffsetX, windowHeight)
    love.graphics.rectangle('fill', windowWidth - windowOffsetX, 0, windowOffsetX, windowHeight)
    love.graphics.rectangle('fill', 0, 0, windowWidth, windowOffsetY)
    love.graphics.rectangle('fill', 0, windowHeight - windowOffsetY, windowWidth, windowOffsetY)
end


